#include <TransformDialog.hpp>
//! libCore
#include <libCore/Entity.hpp>
#include <libCore/Scene.hpp>
#include <libCore/TransformGraphManager.hpp>
#include <libCore/TransformGraphComponent.hpp>
//! ImGui
#include <imgui.h>
//! libUtils
#include <libUtils/Utils.hpp>

TransformDialog::TransformDialog(std::shared_ptr<libCore::TransformGraphManager> const& transformManager)
    : m_transformManager(transformManager)
{
}

std::string TransformDialog::getTitle() const
{
    auto str = std::string();
    if (m_entity.lock())
        str = ": " + m_entity.lock()->getDisplayName();
    return std::string("Transform widget") + str;
}

bool TransformDialog::connect(std::shared_ptr<libCore::Entity> entity, std::shared_ptr<libCore::Scene> scene)
{
    //check if enity is compatible
    if (scene->hasComponent<libCore::TransformGraphComponent>(*entity))
    {
        m_entity = entity;
        m_scene = scene;
        return true;
    }
    return false;
}

void TransformDialog::recordUi()
{
    auto lockedEntity = m_entity.lock();
    auto lockedScene = m_scene.lock();
    auto lockedManager = m_transformManager.lock();

    bool validEntity = lockedEntity && lockedScene && lockedManager;

    if (!validEntity)
    {
        ImGui::TextColored(ImVec4(0, 0.42, 0.43, 1.), "Select model with connect");
        m_entity = {};
        m_scene = {};

        ImGui::BeginDisabled();
        ImGui::Button("Dump object");
        ImGui::EndDisabled();
    }
    else
    {
        if (ImGui::Button("Dump object") && lockedManager)
        {
            auto transformComponent = lockedScene->getComponent<libCore::TransformGraphComponent>(*lockedEntity).lock();
            auto path = transformComponent->getLocalNodePath(0);
            auto topLevelPath = path.substr(0, path.find_last_of("/"));
            libUtils::printOut(lockedManager->toString(topLevelPath, true));
        }
    }
    ImGui::SameLine();
    if (ImGui::Button("Dump tree"))
    {
        libUtils::printOut(lockedManager->toString("/", true));
    }
}
