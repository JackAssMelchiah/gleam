#include <SDL.h> //main func
#include <ModelViewer.hpp>

int main(int argc, char* argv[]) {

	auto modelViewer = new ModelViewer();

	if (!modelViewer->initialize())
	{
		delete modelViewer;
		return 1;
	}

	modelViewer->run();
	delete modelViewer;
    return 0;
}