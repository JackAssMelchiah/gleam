#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_RADIANS
//! glm
#include <glm/gtx/quaternion.hpp>
//! app include
#include <ModelViewer.hpp>
//! libCore
#include <libCore/Core.hpp>
#include <libCore/Window.hpp>
#include <libCore/VertexBuffer.hpp>
#include <libCore/TextureBuffer.hpp>
#include <libCore/ImageLoader.hpp>
#include <libCore/Loader.hpp>
#include <libCore/Scene.hpp>
#include <libCore/Renderer.hpp>
#include <libCore/RenderChain.hpp>
#include <libCore/ForwardRenderingLink.hpp>
#include <libCore/MaterialPhongLayout.hpp>
#include <libCore/MaterialPhongNormalMapLayout.hpp>
#include <libCore/MaterialUnshadedLayout.hpp>
#include <libCore/MaterialUnshadedTexturedLayout.hpp>
#include <libCore/MaterialPBRLayout.hpp>
#include <libCore/MaterialPBRTexturedLayout.hpp>
#include <libCore/RenderComponent.hpp>
#include <libCore/TransformGraphComponent.hpp>
#include <libCore/Camera.hpp>
#include <libCore/FirstPersonHandler.hpp>
#include <libCore/BasicRenderComponent.hpp>
#include <libCore/Picker.hpp>
//! libVulkan
#include <libVulkan/VulkanAll.hpp>
#include <libVulkan/UtilFunctions.hpp>
//! libInputs
#include <libInputs/InputManager.hpp>
//! libUtils
#include <libUtils/Utils.hpp>
//! libUi
#include <libUi/UiClassManager.hpp>
//! glm
#include <glm/gtx/quaternion.hpp>


bool ModelViewer::initialize()
{
	auto init = libCore::ImageLoader::initialize();
	assert(init);

	// specify settings for gleam
	auto settings = std::make_shared<libVulkan::VulkanDefaults>();
	{
		settings->appName = "ModelViewer";
		settings->appVersion = { 0, 0, 1 };
		settings->presentationMode = vk::PresentModeKHR::eImmediate;
		settings->windowSamples = 3;
		settings->validationLayers.push_back("VK_LAYER_KHRONOS_validation"); //synchronization validation
		settings->extensions.push_back(VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME);
	}

	// initialize gleam
	m_gleam = std::make_unique<libCore::Gleam>();
	if (!m_gleam->initialize(false, settings))
		return false;


	//add transform dialog
	m_transformDialog = std::make_shared<TransformDialog>(m_gleam->getTransformGraphManager().lock());
	m_gleam->getUiManager().addUiClass(m_transformDialog);

	//sets up all the rendering primitives
	setUpRendering();

	//conect picking
	setUpPicking();


	//add a fileDialog
	m_fileDialog = std::make_shared<FileDialog>(std::string{ RESOURCES_PATH } + "models", std::vector<std::string>{".glb", ".gltf"});
	m_gleam->getUiManager().addUiClass(m_fileDialog);
	m_fileDialog->connect([&](std::string const& filePath)->void
		{
			libUtils::printOut("Opening " + filePath);

			auto& window = m_gleam->getVulkanManager().getWindow();
			auto device = m_gleam->getVulkanManager().getDevice().lock();
			device->waitIdle();

			// load the scene
			m_importedScene = importScene(filePath);		
			m_importedScene->setRenderAABB(true);

			auto objects = std::vector<std::shared_ptr<libCore::Entity>>();
			m_importedScene->getRenderEntities(objects, true);
			if (!objects.empty())
			{
				//aligns the imported object to grid 
				//sceneObjectsToGrid(*m_importedScene, 4);
				//addInstance(*m_importedScene, objects.at(0), 2);
				//it is nescesary to update the tree after inport or addition of instances, otherwise it will use uninitialized values
				//m_gleam->getTransformGraphManager().lock()->processScene(*m_importedScene);
				//updateAsSphericalHierarchy(*m_importedScene, objects.at(0));

				//all object data needs to be buffered
				m_importedScene->bufferMaterialAndObjectsData();

				m_pbrDialog->connect(objects.at(0), m_importedScene);
				m_transformDialog->connect(objects.at(0), m_importedScene);				
			}

			//when instances are added, they need to be re-binded again
			auto windowExtend = m_gleam->getVulkanManager().getWindow().getExtent();
			m_importedScene->resolutionChanged(windowExtend);

			//scene have changed, picker needs to get it anew
			m_picker->setScene(m_importedScene);
		}
	);
	m_gleam->getUiManager().addUiClass(m_pbrDialog);

	return true;
}

void ModelViewer::setUpRendering()
{
	auto& window = m_gleam->getVulkanManager().getWindow();
	{// sets rendering
		auto device = m_gleam->getVulkanManager().getDevice().lock();
		auto instance = m_gleam->getVulkanManager().getInstance().lock();
		auto ques = m_gleam->getVulkanManager().getQueues().lock();
		auto& surface = window.getSurface();
		auto resolution = window.getExtent();

		m_renderer = std::make_shared<libCore::Renderer>(instance, device, ques->getQueue(vk::QueueFlagBits::eGraphics).lock());

		createCamera(resolution);
			
		createObjectPass();

		createObjects();

		recordStaticScene(resolution);
	}

	window.setResizeCallback([&](vk::Extent2D const& resolution)->void
		{// resize callback -> all memory backings and object build from them must be recreated
			//before recording, descriptors need to be bound
			resolutionChanged(resolution);
			// re-record static scene
			recordStaticScene(resolution);
		}
	);
}

void ModelViewer::createObjects()
{
	//add control dialog for pbr
	m_pbrDialog = std::make_shared<PbrDialog>();

	//import the enviroment scene - wont get changed
	m_environmentScene = std::make_shared<libCore::Scene>(m_renderer, "EnvironmentScene");//importScene(std::string{ RESOURCES_PATH } + "models/sphere_hard.glb");

	//refresh transforms of environment scene
	updateScene(m_environmentScene);
	
	//light for the scene- its for debug - will be there allways, so lets add it to environment
	createAndAddLightDrawable(m_environmentScene);

	//import the scene which will get changed
	m_importedScene = std::make_shared<libCore::Scene>(m_renderer, "importedScene");//importScene(std::string{ RESOURCES_PATH } + "models/sphere_hard.glb"); //"models/sphere_hard.glb"
	m_importedScene->setRenderAABB(true);
	auto objects = std::vector<std::shared_ptr<libCore::Entity>>();
	m_importedScene->getRenderEntities(objects, true);
		//presume now that the loaded model is pbr compatible, add it to pbrDialog
	{
		auto entities = std::vector<std::shared_ptr<libCore::Entity>>();
		m_importedScene->getEntitiesWithComponents<libCore::RenderComponent>(entities);
		if (!entities.empty())
		{
			m_pbrDialog->connect(entities.at(0), m_importedScene);
			m_transformDialog->connect(entities.at(0), m_importedScene);
		}			
	}
	if (!objects.empty())
	{
		//all object data needs to be buffered
		m_importedScene->bufferMaterialAndObjectsData();
	}
	//all object data needs to be buffered
	m_environmentScene->bufferMaterialAndObjectsData();

	//when instances are added, they need to be re-binded again
	auto windowResolution = m_gleam->getWindow()->getWindowResolution();
	resolutionChanged({ uint32_t(windowResolution.x), uint32_t(windowResolution.y) });
}

void ModelViewer::resolutionChanged(vk::Extent2D const& resolution)
{
	getActiveCamera().setPerspectiveProjection(70.f, float(resolution.width) / resolution.height, { 0.01f, 100.f });
	getActiveHandler().resize(glm::ivec2(resolution.width, resolution.height));
	m_renderer->resolutionChanged(resolution);
	m_objectPass->resolutionChanged(resolution);
	if (m_environmentScene)
		m_environmentScene->resolutionChanged(resolution);
	if (m_importedScene)
		m_importedScene->resolutionChanged(resolution);
}

libCore::Handler& ModelViewer::getActiveHandler()
{
	return *m_fpsHandler;
}

libCore::Camera& ModelViewer::getActiveCamera()
{
	return *m_activeCamera;
}

void ModelViewer::setUpPicking()
{
	auto im = m_gleam->getInputManagerWeak().lock();
	auto& window = m_gleam->getWindow();
	auto& camera = m_activeCamera;
	auto& scene = m_importedScene;

	m_picker = std::make_shared<libCore::Picker>(im, window, camera, scene);
}

void ModelViewer::render()
{
	auto& window = m_gleam->getVulkanManager().getWindow();
	//for 0 size window, dont do render
	if (window.getExtent().width == 0 || window.getExtent().height == 0)
		return;
	// before any cpu related stuff should occur, must ensure that gpu has finished execution
	window.getRenderingFence().wait();
	// rendering is done, can update uniforms and textures
	updateUniforms();
	// create dynamic buffer which will record all dynamic objects
	auto dynamicPrimaryBuffer = m_renderer->getDynamicCommandBuffer(vk::CommandBufferLevel::ePrimary).lock();
		
	// record dynamic command buffers
	if (m_importedScene)
	{
		recordDynamicScene(*dynamicPrimaryBuffer, window.getExtent());
	}
	//now submit all the recorded dynamic command buffer + the recorded static one
	auto pcb = m_renderer->getPersistentCommandBuffer().lock();
	m_renderer->submit(std::vector<std::reference_wrapper<libVulkan::VulkanCommandBuffer>>{ *pcb });
	// forced que sync in submit, so static objects are guaranteed to be rendered before dynamic rendering TODO: sync via semaphores
	if (m_importedScene)
		m_renderer->submit(std::vector<std::reference_wrapper<libVulkan::VulkanCommandBuffer>>{ *dynamicPrimaryBuffer });
			
	auto& uiManager = m_gleam->getUiManager();
	//now perform rendering of UI - Gpu waits till previous submit happened
	uiManager.render(m_objectPass->getOutputAttachment().lock(), window.getExtent(), {});
	
	//now present the recorded fbo - wait for ui to finish rendering
	window.present(m_objectPass->getOutputAttachment().lock(), 0, { uiManager.getRenderingFinishedSemaphore() });
	//tells the renderer that frame is done, dynamic resources will be freed automatically 
	m_renderer->finalizeFrame();
}

void ModelViewer::createCamera(vk::Extent2D const& resolution)
{
	auto aspectRatio = resolution.width / resolution.height;
	//m_activeCamera = std::make_shared<libCore::Camera>(75., aspectRatio, glm::vec2(0.1, 100.));
	//correct way to set the ortho camera:
	m_activeCamera = std::make_shared<libCore::Camera>(glm::vec2(-aspectRatio, aspectRatio), glm::vec2(-1, 1), glm::vec2(-1, 1));

	//set up fps handler
	//m_fpsHandler = std::make_shared<libCore::FirstPersonHandler>(glm::vec3(0.f, 0.f, 10.f), glm::vec3(0.f, 0.f, -1.f));
	m_fpsHandler = std::make_shared<libCore::FirstPersonHandler>(glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 0.f, -1.f));

	//add handler to input manager, so it can process the inputs
	m_gleam->getInputManager().addHandler(m_fpsHandler);
}

void ModelViewer::createAndAddLightDrawable(std::shared_ptr<libCore::Scene> const& scene)
{
	auto cubeVertices = std::vector<libUtils::Shape::ColoredVertex>();
	libUtils::Shape::cube(cubeVertices);
	m_light = std::make_shared<libCore::BasicRenderComponent>(m_renderer, scene, cubeVertices, "Light");
}

void ModelViewer::recordStaticScene(vk::Extent2D const& resolution)
{
	auto instance = m_gleam->getVulkanManager().getInstance().lock();
	auto& defaults = instance->getDefaults();
	
	//release resources recorded in the persitent command buffer
	m_renderer->releasePersistentCommandBuffer();
	//given command buffer is empty
	auto pcb = m_renderer->getPersistentCommandBuffer().lock();

	pcb->beginRecord(vk::CommandBufferUsageFlags());
	pcb->beginRendering({ m_objectPass->getOutputAttachment().lock()}, m_objectPass->getOutputDepthStencilAttachment().lock(), resolution, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eStore, false, "Static scene render");
	// this is first renderpass, so clear the attachment
	pcb->recordClearAttachments({ m_objectPass->getOutputAttachment().lock()}, m_objectPass->getOutputDepthStencilAttachment().lock(), defaults.clearColor, defaults.clearDepth);

	if (m_environmentScene)
		m_environmentScene->recordAll(*pcb, *m_objectPass);
	pcb->insertDebugMessage("BOBR3");
	pcb->endRendering();
	pcb->endRecord();
}

void ModelViewer::recordDynamicScene(libVulkan::VulkanCommandBuffer& commandBuffer, vk::Extent2D const& resolution)
{
	auto count = m_importedScene ? m_importedScene->getRenderComponentObjectCount(): 0;
	auto buffers = std::vector<std::reference_wrapper<libVulkan::VulkanCommandBuffer>>();
	//TODO: do this in paralel
	for (int i = 0; i < count; ++i)
	{
		auto dcb = m_renderer->getDynamicCommandBuffer(vk::CommandBufferLevel::eSecondary).lock();
		dcb->beginRecord(*m_objectPass->getRenderingInfo(), vk::CommandBufferUsageFlags{vk::CommandBufferUsageFlagBits::eOneTimeSubmit | vk::CommandBufferUsageFlagBits::eRenderPassContinue});
		m_importedScene->recordSingle(*dcb, i, *m_objectPass);
		dcb->insertDebugMessage("BOBR2");
		dcb->endRecord();
		buffers.push_back(*dcb);
	}

	//now do a record of the final command buffer
	commandBuffer.beginRecord({ vk::CommandBufferUsageFlagBits::eOneTimeSubmit });
	commandBuffer.beginRendering({ m_objectPass->getOutputAttachment().lock()}, m_objectPass->getOutputDepthStencilAttachment().lock(), resolution, vk::AttachmentLoadOp::eLoad, vk::AttachmentStoreOp::eStore, true, "dynamic render");
	commandBuffer.executeSecondaryBuffers(buffers);
	commandBuffer.endRendering();
	commandBuffer.endRecord();
}

void ModelViewer::createObjectPass()
{
	auto device = m_gleam->getVulkanManager().getDevice().lock();
	auto instance = m_gleam->getVulkanManager().getInstance().lock();

	auto& window = m_gleam->getVulkanManager().getWindow();

	//m_chain = std::make_shared<libCore::RenderChain>();

	//need rending info, all material layouts have same rendering info compatible
	auto layout = std::make_shared<libCore::MaterialUnshadedLayout>(instance, device); // TODO do some forward batches?
	m_objectPass = std::make_shared<libCore::ForwardRenderingLink>(m_renderer, layout->getRenderingInfo(), m_activeCamera);
}

void ModelViewer::updateUniforms()
{
	auto& camera = getActiveCamera();

	//update camera
	getActiveHandler().handle(camera);
	//Renderer resources
	m_renderer->bufferResources();
	//Pass resources
	m_objectPass->bufferResources();
	//Material resources
	updateMaterialResources();
	//Model resources
	updateObjectResources();
}

void ModelViewer::updateMaterialResources()
{
	// material resources - camera position and light position 
	auto cameraPostion = m_activeCamera->getPosition();
	auto lightPosition = m_lightPosition;
	{
		if (m_environmentScene)
		{
			auto& materialResources = m_environmentScene->getMaterialResources();
			materialResources.setInstanceResource("CAMERA_POSITION", glm::vec4(cameraPostion, 0.f), materialResources.getBaseInstance());
			materialResources.setInstanceResource("LIGHT_POSITION", glm::vec4(lightPosition, 0.f), materialResources.getBaseInstance());
		}
		if (m_importedScene)
		{
			auto& materialResources = m_importedScene->getMaterialResources();
			materialResources.setInstanceResource("CAMERA_POSITION", glm::vec4(cameraPostion, 0.f), materialResources.getBaseInstance());
			materialResources.setInstanceResource("LIGHT_POSITION", glm::vec4(lightPosition, 0.f), materialResources.getBaseInstance());
		}
	}
}

void ModelViewer::updateObjectResources()
{
	if (m_pbrDialog && m_pbrDialog->updateEntity())
	{
		m_importedScene->bindObjectResources(m_pbrDialog->getHandledComponent()->getMaterialLayout()->getType());
	}
		
	if(m_light)
		updateLight();

	if (m_environmentScene)
	{
		//m_gleam->getTransformGraphManager().lock()->processScene(*m_environmentScene);
		m_environmentScene->bufferMaterialData();
	}

	updateScene(m_importedScene);
}

void ModelViewer::updateScene(std::shared_ptr<libCore::Scene> const& scene)
{
	if (scene)
	{
		auto entities = std::vector<std::shared_ptr<libCore::Entity>>();
		scene->getRenderEntities(entities, true);

		scene->bufferMaterialData();
		m_gleam->getTransformGraphManager().lock()->processScene(*scene);

		for (auto& entity : entities)
		{
			auto renderComponent = scene->getComponent<libCore::RenderComponent>(*entity).lock();
			auto transformComponent = scene->getComponent<libCore::TransformGraphComponent>(*entity).lock();
			auto layout = renderComponent->getMaterialLayout();
			auto instanceHandle = renderComponent->getResources().getBaseInstance();
			for (auto i = 0; i < renderComponent->getInstanceCount(); ++i)
			{
				renderComponent->getResources().setInstanceResource("MODEL_MATRIX", transformComponent->getWorldMatrix(i), instanceHandle);
				//model resource was changed, need to buffer it
				layout->bufferObjectResources(renderComponent->getResources());
				instanceHandle.nextInstance();
			}
		}
		if (scene->getRenderAABB())
		{
			scene->updateAABBRenderComponent();
		}
	}
}

std::shared_ptr<libCore::Scene> ModelViewer::importScene(std::string const& path) const
{	
	auto gltfLoader = libCore::Loader(m_renderer, m_gleam->getTransformGraphManager());
	gltfLoader.load(path, libCore::Loader::Material::PBR_TEXTURED);
	return gltfLoader.getScene();
}

void ModelViewer::updateLight()
{
	//compute light position
	auto radius = 10.f;
	auto center = glm::vec3(0.f, 0.f, 10.f);
	auto time = m_renderer->getTime() * 0.001; //in sec

	m_lightPosition = glm::vec3(glm::sin(time) * radius, 0.f, glm::cos(time) * radius);
	auto scale = glm::vec3(1.f, 1.f, 1.f);
	auto modelMatrix = glm::mat4(glm::scale(glm::translate(glm::identity<glm::mat4>(), m_lightPosition), scale));

	m_light->setMatrix(modelMatrix, 0);
	m_light->bufferData();
}

bool ModelViewer::run()
{
	while (true)
	{
		auto disableHandlers = m_gleam->getUiManager().mouseHoversOverUi();
		
		if (m_gleam->getInputManager().processAllInputs(disableHandlers))
			break;
		render();
	}
	m_gleam->getVulkanManager().getDevice().lock()->waitIdle();

	libCore::ImageLoader::quit();
	return true;
}