#version 450

layout (std140, binding = 0) uniform UniformBufferObject
{
    mat4 modelMatrix;
} ubo;

layout ( push_constant) uniform PushConstants
{
    mat4 viewMatrix;
    mat4 projectionMatrix;
} pc;

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 color;
layout(location = 2) in vec2 uv;

layout(location = 0) out vec2 uvOut;

void main()
 {
    uvOut = uv;
    gl_Position = pc.projectionMatrix * pc.viewMatrix * ubo.modelMatrix * vec4(vertex, 1.0);
}