#version 450

layout(location = 0) in vec2 uvs;
layout(binding = 1) uniform sampler2D textureColor;

layout(location = 0) out vec4 outColor;

void main()
{
    outColor = texture(textureColor, uvs);
}