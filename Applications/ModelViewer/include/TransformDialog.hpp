#pragma once
//! libUi
#include <libUi/UiClass.hpp>
//! libCore
#include <libCore/ShaderResources.hpp>
//! glm
#include <glm/glm.hpp>
//! Std
#include <string>
#include <memory>

namespace libCore
{
	class Entity;
	class Scene;
	class RenderComponent;
	class TransformGraphManager;
}

class TransformDialog : public libUi::UiClass
{
public:
	TransformDialog(std::shared_ptr<libCore::TransformGraphManager> const& manager);
	~TransformDialog() = default;
	//! Returs class title
	std::string getTitle() const override;
	//! Connects the entity which is to be handled
	bool connect(std::shared_ptr<libCore::Entity> entity, std::shared_ptr<libCore::Scene> scene);

protected:;
	void recordUi() override;

protected:
	std::weak_ptr<libCore::Entity> m_entity;
	std::weak_ptr<libCore::Scene> m_scene;
	std::weak_ptr<libCore::TransformGraphManager> m_transformManager;
};