#pragma once
//!Vulkan
#include <vulkan/vulkan.hpp>
//! APP
#include <FileDialog.hpp>
#include <PbrDialog.hpp>
#include <TransformDialog.hpp>
//! libCore
#include <libCore/Core.hpp>
//! libUtils
#include <libUtils/Time.hpp>
//! glm
#include <glm/glm.hpp>

namespace libCore
{
	class Scene;
	class Renderer;
	class ForwardRenderingLink;
	class RenderComponent;
	class Camera;
	class FirstPersonHandler;
	class Handler;
	class BasicRenderComponent;
	class Picker;
}

namespace libVulkan
{
	class VulkanCommandBuffer;
	class VulkanFence;
}

class ModelViewer
{
public:
	ModelViewer() = default;
	~ModelViewer() = default;
	bool initialize();
	bool run();

protected:
	//! Testing method, will later be migrated into libCore
	void setUpRendering();

	//! testing mesh creation for light
	void createAndAddLightDrawable(std::shared_ptr<libCore::Scene> const& scene);
	//! Records static scene - use persistent command buffer for it, since theese models wont get changed ever
	void recordStaticScene(vk::Extent2D const& resolution);
	//! Records dynamic scene
	void recordDynamicScene(libVulkan::VulkanCommandBuffer& commandBuffer, vk::Extent2D const& resolution);
	//! Creates object render pass
	void createObjectPass();

	//! Renders the scene
	void render();
	//! Creates default camera
	void createCamera(vk::Extent2D const&);
	//! Updates all per frame data for imported and environment models
	void updateUniforms();
	//! Update just material resources - since this is modelviewer and imported mesh could have multiple mats, need to update them
	void updateMaterialResources();
	//! Updates object resources
	void updateObjectResources();
	//! Light is just object which moves
	void updateLight();

	void updateScene(std::shared_ptr<libCore::Scene> const& scene);

	//! Imports scene, and automatically creates pipeline for objects within scene
	std::shared_ptr<libCore::Scene> importScene(std::string const& path) const;

	void createObjects();
	//! When resolution gets changed, descriptor set will get updated again
	void resolutionChanged(vk::Extent2D const& resolution);

	libCore::Handler& getActiveHandler();
	libCore::Camera& getActiveCamera();

	void setUpPicking();

protected:
	std::unique_ptr<libCore::Gleam> m_gleam;
	std::shared_ptr<FileDialog> m_fileDialog;
	std::shared_ptr<PbrDialog> m_pbrDialog;
	std::shared_ptr<TransformDialog> m_transformDialog;

	//! Scenes to render
	std::shared_ptr<libCore::Scene> m_importedScene;
	std::shared_ptr<libCore::Scene> m_environmentScene;

	//! Camera && handlers
	std::shared_ptr<libCore::Camera> m_activeCamera;
	std::shared_ptr<libCore::FirstPersonHandler> m_fpsHandler;

	//! Renderer
	std::shared_ptr<libCore::Renderer> m_renderer;
	//! Object render pass
	std::shared_ptr<libCore::ForwardRenderingLink> m_objectPass;

	//! Fence for cpu work synchronization
	std::shared_ptr<libVulkan::VulkanFence> m_sceneComplete;

	//! Stuff used with viewer
	std::shared_ptr<libCore::BasicRenderComponent> m_light;
	glm::vec3 m_lightPosition;

	std::shared_ptr<libCore::Picker> m_picker;
};