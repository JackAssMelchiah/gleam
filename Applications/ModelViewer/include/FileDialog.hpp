#pragma once
//! libUi
#include <libUi/UiClass.hpp>
//! Std
#include <string>
#include <filesystem>
#include <functional>
//! imgui
#include <imgui.h>

class FileDialog : public libUi::UiClass
{
public:
	FileDialog(std::string const& filePath, std::vector<std::string> const& suffixFilter = {});
	~FileDialog() = default;
	//! Returs class title
	std::string getTitle() const override;
	//! Connects the callback to happen when user accepts dialog
	void connect(std::function<void(std::string const&)> callback);

protected:
	void recordUi() override;
	bool surpassedExtensionCheck(std::filesystem::path const& file) const;
	void demoUi();

protected:
	std::filesystem::path m_currentPath;
	std::vector<std::string> m_suffixes;

	std::function<void(std::string const&)> m_callback;

	ImVec4 m_folderColor;
};