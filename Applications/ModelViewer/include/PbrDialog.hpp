#pragma once
//! libUi
#include <libUi/UiClass.hpp>
//! libCore
#include <libCore/ShaderResources.hpp>
//! glm
#include <glm/glm.hpp>
//! Std
#include <string>
#include <vector>
#include <memory>

namespace libCore
{
	class Entity;
	class Scene;
	class RenderComponent;
}

class PbrDialog : public libUi::UiClass
{
public:
	PbrDialog();
	~PbrDialog() = default;
	//! Returs class title
	std::string getTitle() const override;
	//! Connects the entity which is to be handled
	bool connect(std::shared_ptr<libCore::Entity> entity, std::shared_ptr<libCore::Scene> scene);
	//! Writes all entity data into gpu, returns true if instance was added or removed, and thus buffers might be different and rebind is needed
	bool updateEntity();

	std::shared_ptr<libCore::RenderComponent> getHandledComponent();

protected:
	using InstanceData = struct ID
	{
		glm::vec3 albedoColor;
		float occlusion;
		float metallicness;
		float roughness;
	};

protected:
	PbrDialog::ID generateInstanceData(libCore::ShaderResources::Instance instance);
	void recordUi() override;
	void demoUi();

protected:
	std::vector<InstanceData> m_instanceData;
	std::weak_ptr<libCore::Entity> m_entity;
	std::weak_ptr<libCore::Scene> m_scene;

	bool m_addInstance;
	bool m_removeInstance;

	bool m_skeletonCapability;
	bool m_showSkeleton;
};